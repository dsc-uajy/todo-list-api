<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->get('todos', 'TodoController@index');
$router->post('todos', 'TodoController@store');
$router->get('todos/{id}', 'TodoController@get');
$router->post('todos/{id}', 'TodoController@addItem');
$router->patch('todos/{id}', 'TodoController@update');
$router->post('items/{id}', 'TodoController@checkItem');
$router->delete('todos/{id}', 'TodoController@destroy');
