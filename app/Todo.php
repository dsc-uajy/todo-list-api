<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Todo extends Model
{
    /**
     * Atribut-atribut yang ada di tabel.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    /**
     * Fetch semua Item yang dimiliki Todo ini.
     *
     * @return array
     */
    public function items()
    {
        return $this->hasMany(Item::class);
    }
}
