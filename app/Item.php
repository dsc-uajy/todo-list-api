<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    /**
     * Atribut-atribut yang ada di tabel.
     *
     * @var array
     */
    protected $fillable = [
        'todo_id',
        'name',
        'done',
    ];

    /**
     * Atribut yang di-hide.
     *
     * @var array
     */
    protected $hidden = [
        'todo_id',
    ];

    /**
     * done di-cast menjadi boolean (awalnya integer).
     *
     * @var array
     */
    protected $casts = [
        'done' => 'boolean',
    ];
}
