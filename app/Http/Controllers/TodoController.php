<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Todo;
use App\Item;

class TodoController extends Controller
{
    /**
     * Fetch semua Todo beserta Item yang dimilikinya
     *
     * @return Illuminate\Http\Response
     */
    public function index()
    {
        return Todo::with('items')->get();
    }

    /**
     * Menyimpan Todo baru.
     *
     * @param Request $request
     * @return Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'items' => 'required|array',
            'items.*.name' => 'required',
        ]);

        $todo = Todo::create([
            'name' => $request->get('name'),
        ]);

        $items = collect($request->get('items'))
            ->map(function ($item) {
                return new Item([
                    'name' => $item['name'],
                ]);
            });

        $todo->items()->saveMany($items);

        return Todo::with('items')->find($todo->id);
    }

    /**
     * Fetch 1 Todo beserta Item-Item di dalamnya.
     *
     * @param int $id
     * @return Illuminate\Http\Response
     */
    public function get($id)
    {
        $todo = Todo::with('items')->find($id);

        if (is_null($todo)) {
            return response()->json('todo_not_found', 404);
        }

        return $todo;
    }

    public function update(Request $request, $id)
    {
        $todo = Todo::with('items')->find($id);

        if (is_null($todo)) {
            return response()->json('todo_not_found', 404);
        }

        $this->validate($request, [
            'name' => 'required',
        ]);

        $todo->update(['name' => $request->get('name')]);

        return $todo;
    }

    /**
     * Menambah Item baru ke Todo.
     *
     * @param Request $request
     * @param int $id
     * @return Illuminate\Http\Response
     */
    public function addItem(Request $request, $id)
    {
        $todo = Todo::with('items')->find($id);

        if (is_null($todo)) {
            return response()->json('todo_not_found', 404);
        }

        $this->validate($request, [
            'name' => 'required',
        ]);

        $item = new Item(['name' => $request->get('name')]);
        $todo->items()->save($item);

        return response()->json();
    }

    /**
     * Menyentang Item.
     *
     * @return Illuminate\Http\Response
     */
    public function checkItem($id)
    {
        $item = Item::find($id);

        if (is_null($item)) {
            return response()->json('item_not_found', 404);
        }

        $item->update(['done' => true]);

        return response()->json();
    }

    /**
     * Menghapus Todo.
     *
     * @param int $id
     * @return Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $todo = Todo::find($id);

        if (is_null($todo)) {
            return response()->json('todo_not_found', 404);
        }

        $todo->items->each->delete();
        $todo->delete();

        return response()->json();
    }
}
